package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.DetalleVenta;

@Repository
public interface IDetalleVentaDAO extends JpaRepository<DetalleVenta, Integer>{

}
